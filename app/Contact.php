<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'person_id', 'phone',
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }
}
